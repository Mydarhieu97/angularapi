using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DTHAPI.Data;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace DTHAPI.Helpers
{
    public class LogUserActivity : IAsyncActionFilter
    {
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var contextResult = await next();

            var userId = int.Parse(contextResult.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var repo = contextResult.HttpContext.RequestServices.GetService<IDatingRepository>();

            var user = await repo.GetUser(userId);

            user.LastActive = DateTime.Now;
            
            await repo.SaveAll();
        }
    }
}