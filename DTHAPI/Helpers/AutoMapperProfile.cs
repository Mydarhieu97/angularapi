using System.Linq;
using AutoMapper;
using DTHAPI.DTO;
using DTHAPI.Models;

namespace DTHAPI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserForListDTO>()
                .ForMember(dest => dest.PhotoURL, opt => {
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
                .ForMember(dest => dest.Age, opt => {
                    opt.ResolveUsing(d => d.DateOfBirth.CalculateAge());
                })
                .ForMember(dest => dest.CityId, opt => {
                    opt.MapFrom(src => src.CityId);
                });
                

            CreateMap<User, UserForDetailDTO>()
                .ForMember(dest => dest.PhotoURL, opt => {
                    opt.MapFrom(src => src.Photos.FirstOrDefault(p => p.IsMain).Url);
                })
                .ForMember(dest => dest.Age, opt => {
                    opt.ResolveUsing(d => d.DateOfBirth.CalculateAge());
                })
                .ForMember(dest => dest.CityId, opt => {
                    opt.MapFrom(src => src.CityId);
                });
            CreateMap<Photo, PhotoForDetailDTO>();
            CreateMap<UserForUpdateDTO, User>();
            CreateMap<RoleForCliDTO, Role>();
            CreateMap<UserForDeleteDTO, User>();
            CreateMap<Photo, PhotoForReturnDTO>();
            CreateMap<PhotoForCreationDTO, Photo>();
            CreateMap<CityForCreateDTO, City>();
            CreateMap<CityForGetDTO, City>();
            CreateMap<City, CityRenewDTO>()
            .ForMember(dest => dest.Id, opt => {
                    opt.ResolveUsing(d => d.CityId);
                })
                .ForMember(dest => dest.Text, opt => {
                    opt.MapFrom(src => src.Title);
                });
            CreateMap<UserForRegisterDTO, User>();
        }
    }
}