using System.Threading.Tasks;
using DTHAPI.Data;
using DTHAPI.Models;
using DTHAPI.DTO;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace DTHAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        public IAuthorRepository _repo;
        public IConfiguration _config;
        public IRoleRepository _roleRepo;
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorRepository repo, IConfiguration config, IRoleRepository roleRepo, DataContext context, IMapper mapper)
        {
            _roleRepo = roleRepo;
            _context = context;
            _mapper = mapper;
            _repo = repo;
            _config = config;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register (UserForRegisterDTO userForRegisterDTO)
        {
            userForRegisterDTO.UserName = userForRegisterDTO.UserName.ToLower();

            if(await _repo.CheckUser(userForRegisterDTO.UserName)) return BadRequest("User's Name is Already Exist");

            var userToCreate = _mapper.Map<User>(userForRegisterDTO);

            var CreateUser = await _repo.Register(userToCreate, userForRegisterDTO.PassWord);

            var userForReturn = _mapper.Map<UserForDetailDTO>(CreateUser);

            return CreatedAtRoute("GetUser", new {controller = "Users", id = CreateUser.Id}, userForReturn);
        }

        public static bool IsDateTime(string txtDate)
        {
            DateTime tempDate;
            return DateTime.TryParse(txtDate, out tempDate);
        }
        
        [HttpPost("login")]
        public async Task<IActionResult> Login (UserForLoginDTO userForLoginDTO)
        {
            var userNameRepo = await _repo.Login(userForLoginDTO.Username, userForLoginDTO.Password);
            
            if(userNameRepo == null)
            return Unauthorized();
            else
            {
            var getUserId = await _context.Users.FirstOrDefaultAsync(u => u.UserName == userForLoginDTO.Username);
            var roleName = _roleRepo.GetRole1(getUserId.Id);
            List<string> strings = roleName.Select(s => s.Name).ToList();
            string combindedString = string.Join( ",", strings);

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, userNameRepo.Id.ToString()),
                new Claim(ClaimTypes.Name, userNameRepo.UserName),
                new Claim(ClaimTypes.Role, combindedString),
            }; 

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSetting:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescreptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandle = new JwtSecurityTokenHandler();

            var token = tokenHandle.CreateToken(tokenDescreptor);
            
            var user = _mapper.Map<UserForListDTO>(userNameRepo);
            return Ok(
                new{
                    token=tokenHandle.WriteToken(token), user.PhotoURL, user.Status, user.Gender
                }
            );
            }

        }
        
        
    }
}