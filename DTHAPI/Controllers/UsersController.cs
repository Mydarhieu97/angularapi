using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using DTHAPI.Data;
using DTHAPI.DTO;
using DTHAPI.Helpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.Controllers
{
    [ServiceFilter(typeof(LogUserActivity))]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IDatingRepository _repo;
        private readonly IMapper _mapper;

        public UsersController(IDatingRepository repo, IMapper mapper)
        {
            _repo = repo;
            _mapper = mapper;
        }
        [HttpGet]
        public async Task<IActionResult> GetUsers([FromQuery]UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var userFromRepo = await _repo.GetUser(currentUserId);

            userParams.UserId = currentUserId;

             if(userParams.Gender == null)
             {
                userParams.Gender = userFromRepo.Gender == 1 ? 0 : 1;
             }
            
            var users = await _repo.GetUsersActive(userParams);

            var userForReturn = _mapper.Map<IEnumerable<UserForListDTO>>(users);

            Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

            return Ok(userForReturn);
        }

        [HttpGet("Inactive")]
        public async Task<IActionResult> GetUsersInactive([FromQuery]UserParams userParams)
        {
            var currentUserId = int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value);

            var userFromRepo = await _repo.GetUser(currentUserId);

            userParams.UserId = currentUserId;
            
            var users = await _repo.GetUsersInactive(userParams);

            var userForReturn = _mapper.Map<IEnumerable<UserForListDTO>>(users);

            Response.AddPagination(users.CurrentPage, users.PageSize, users.TotalCount, users.TotalPages);

            return Ok(userForReturn);
        }



        [HttpGet("{id}", Name="GetUser")]
        public async Task<IActionResult> GetUser(int id){
            var user = await _repo.GetUser(id);
            var userForReturn = _mapper.Map<UserForDetailDTO>(user);
            return Ok(userForReturn);
        }

        [HttpDelete("delete/{id}")]
        public void Delete(int id)
        {
            _repo.DeleteById(id);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserForUpdateDTO userForUpdateDTO)
        {
            if(id != int.Parse(User.FindFirst(ClaimTypes.NameIdentifier).Value))
            return Unauthorized();

            var userFromRepo = await _repo.GetUser(id);

            _mapper.Map(userForUpdateDTO, userFromRepo);

            if(await _repo.SaveAll())
            return NoContent();

            throw new Exception($"Updating user {id} failed to save");

        }


        
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            UserForDeleteDTO userForDeleteDTO = new UserForDeleteDTO();
            var userFromRepo = await _repo.GetUser(id);

            userForDeleteDTO.Status = UserStatus.Inactive;

            _mapper.Map(userForDeleteDTO, userFromRepo);

            if(await _repo.SaveAll())
                return StatusCode(200);

            throw new Exception($"Deleting user {id} failed to save");

        }

        [HttpDelete("enable/{id}")]
        public async Task<IActionResult> EnableUser(int id)
        {
            UserForDeleteDTO userForDeleteDTO = new UserForDeleteDTO();
            var userFromRepo = await _repo.GetUser(id);

            userForDeleteDTO.Status = UserStatus.Active;

            _mapper.Map(userForDeleteDTO, userFromRepo);

            if(await _repo.SaveAll())
                return StatusCode(200);

            throw new Exception($"Enable user {id} failed to save");

        }

    }
}