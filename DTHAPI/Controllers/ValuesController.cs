﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DTHAPI.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using DTHAPI.DTO;
using AutoMapper;
using DTHAPI.Models;

namespace DTHAPI.Controllers
{
    // http://localhost:5000/api/values
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly DataContext _context;
        private readonly IDatingRepository _repo;
        private readonly IMapper _mapper;

        public ValuesController(DataContext context, IDatingRepository repo, IMapper mapper)
        {
            _context = context;
            _repo = repo;
            _mapper = mapper;
        }
        // GET api/values
        // [HttpGet]
        // public async Task<IActionResult> GetValues()
        // {
        //     var movie = await _context.Movies.ToListAsync();
        //     return Ok(movie);
        // }

        // // GET api/values/5
        // [HttpGet("{id}")]
        // public async Task<IActionResult> GetValues(int id)
        // {
        //     var movie = await _context.Movies.Where(m => m.Id == id).FirstOrDefaultAsync();
        //     return Ok(movie);
        // }
        // GET api/values
        [AllowAnonymous]
        [HttpGet]
        // public async Task<IActionResult> GetGenres()
        // {
        //     var genre = await _context.Users.ToListAsync();
        //     return Ok(genre);
        // }

        public async Task<IActionResult> GetCity()
        {
            var city = await _context.Cities.ToListAsync();
            return Ok(city);
        }
        [AllowAnonymous]
        [HttpGet("citynew")]
        public async Task<IActionResult> GetCityNew()
        {
            var city = await _context.Cities.ToListAsync();
            var cityForReturn = _mapper.Map<IEnumerable<CityRenewDTO>>(city);
            return Ok(cityForReturn);
        }

        // GET api/values/5
        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCity1(int id)
        {
            var user =await _context.Users.FirstOrDefaultAsync(u => u.Id == id);
            var city = await _context.Cities.FirstOrDefaultAsync(c => c.CityId == user.CityId);
            var cityForReturn = _mapper.Map<CityForGetDTO>(city);
            return Ok(cityForReturn);
        }
        // public async Task<IActionResult> GetGenres(int id)
        // {
        //     var genre = await _context.Genres.Where(m => m.Id == id).FirstOrDefaultAsync();
        //     return Ok(genre);
        // }
        // POST api/values
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] List<City> city)
        {
            foreach(var item in city)
            {
            await _context.Cities.AddAsync(item);
            }

            await _context.SaveChangesAsync();
            
            
            return StatusCode(200);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
