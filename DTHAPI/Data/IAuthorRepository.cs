namespace DTHAPI.Data   
{
    using System.Threading.Tasks;
    using DTHAPI.Models;
    public interface IAuthorRepository
    {
         Task<User> Register (User user, string password);
         Task<User> Login (string username, string password);
         Task<bool> CheckUser (string username);
    }
}