using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTHAPI.DTO;
using DTHAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace DTHAPI.Data
{
    public class RoleRepository : IRoleRepository
    {
        private readonly DataContext _context;
        public RoleRepository(DataContext context)
        {
            _context = context;

        }
        public async Task<Role> GetRole(int id)
        {
            var roleId = await _context.UserRoles.FirstOrDefaultAsync(r => r.UserId == id);
            var rID = roleId.RoleId;
            var roleName = await _context.Roles.FirstOrDefaultAsync(n => n.Id == rID);
            return roleName;
        }
        public List<RoleForCliDTO> GetRole1(int id)
        {
            var role = from r in _context.Roles
                          join ur in _context.UserRoles on r.Id equals ur.RoleId
                          join u in _context.Users on ur.UserId equals u.Id
                          select new {r.Name, u.Id};
            if (id != null)
            {
                role = role.Where(x => x.Id == id);
            }
            List<RoleForCliDTO> roleForCli = new List<RoleForCliDTO>();
            foreach(var item in role)
            {
                RoleForCliDTO temp = new RoleForCliDTO();
                temp.Name = item.Name;
                roleForCli.Add(temp);
            }
            return roleForCli;
        }
        public async Task<IEnumerable<Role>> GetRoles()
        {
            var role = await _context.Roles.ToListAsync();
            return role;
        }
        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }
    }
}
