using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DTHAPI.Models;
using Microsoft.EntityFrameworkCore;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.Data
{
    public class AuthorRepository : IAuthorRepository
    {
        public DataContext _context;
        public AuthorRepository(DataContext context)
        {
            _context = context;

        }
        public async Task<bool> CheckUser(string username)
        {
            if(await _context.Users.AnyAsync(x => x.UserName == username)) return true;
            return false;
        }

        public async Task<User> Login(string username, string password)
        {
            var user  = await _context.Users.Include(p => p.Photos).FirstOrDefaultAsync(x => x.UserName == username);
            if(user == null)
            return null;

            if(!VerifyPassWordHash(password, user.PassWordHash, user.PassWordSalt))
            return null;
            else
            return user;
        }

        private bool VerifyPassWordHash(string password, byte[] passWordHash, byte[] passWordSalt)
        {
            using (var hWin = new System.Security.Cryptography.HMACSHA512(passWordSalt))
            {
                var computedHash = hWin.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for(int i=0;i<computedHash.Length;i++){
                    if(computedHash[i] != passWordHash[i]) return false;

                }
            }
            return true;
        }

        public async Task<User> Register(User user, string password)
        {
            byte[] passwordHash, passwordSalt;
            CreatePassWordHash(password, out passwordHash, out passwordSalt);
            user.PassWordHash = passwordHash;
            user.PassWordSalt = passwordSalt;
            user.CreateAt = DateTime.Now;
            user.KnownAs = user.UserName;
            user.LastActive = DateTime.Now;
            user.Status = UserStatus.Active;
            await _context.Users.AddAsync(user);
            await _context.SaveChangesAsync();
            var userRole  = await _context.Users.FirstOrDefaultAsync(x => x.UserName == user.UserName);
            if(userRole != null)
            {
                UserRole _userrole = new UserRole();
                var userID = userRole.Id;
                var userRID = 3;
                _userrole.UserId = userID;
                _userrole.RoleId = userRID;
                await _context.UserRoles.AddAsync(_userrole);

            }
            await _context.SaveChangesAsync();
            return user;
        }

        private void CreatePassWordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hWin = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hWin.Key;
                passwordHash = hWin.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
            
        }

    }
}