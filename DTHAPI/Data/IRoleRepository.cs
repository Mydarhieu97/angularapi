using System.Collections.Generic;
using System.Threading.Tasks;
using DTHAPI.DTO;
using DTHAPI.Models;

namespace DTHAPI.Data
{
    public interface IRoleRepository
    {
        Task<Role> GetRole(int id);
        List<RoleForCliDTO> GetRole1(int id);
        Task<IEnumerable<Role>> GetRoles();
        Task<bool> SaveAll();
        
    }
}
