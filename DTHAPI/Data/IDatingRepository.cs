using System.Collections.Generic;
using System.Threading.Tasks;
using DTHAPI.Helpers;
using DTHAPI.Models;

namespace DTHAPI.Data
{
    public interface IDatingRepository
    {
        void Add<T>(T entity) where T: class;
        void Update<T>(T entity) where T: class;
        void Delete<T>(T entity) where T: class;
        void DeleteById(int id);
        void GetById<T>(T entity) where T: class;
        void GetAll<T>(T entity) where T: class;
        Task<bool> SaveAll();
        Task<PagedList<User>> GetUsers(UserParams userParams);
        Task<PagedList<User>> GetUsersActive(UserParams userParams);
        Task<PagedList<User>> GetUsersInactive(UserParams userParams);
        Task<User> GetUser(int id);
        Task<Photo> GetPhoto(int id);
        Task<Photo> GetMainPhotoForUser(int userId);
    } 
}