using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DTHAPI.Helpers;
using DTHAPI.Models;
using Microsoft.EntityFrameworkCore;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.Data
{
    public class DatingRepository : IDatingRepository
    {
        private readonly DataContext _context;
        private readonly IRoleRepository _role;
        public DatingRepository(DataContext context, IRoleRepository role)
        {
            _role = role;
            _context = context;

        }
        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }
        public void DeleteById(int id)
        {
            User user = _context.Users.Where(x => x.Id == id).FirstOrDefault();
            if (user != null)
            {
                _context.Remove(user);
                _context.SaveChanges();
            }
        }

        public void GetAll<T>(T entity) where T : class
        {
            throw new System.NotImplementedException();
        }

        public void GetById<T>(T entity) where T : class
        {
            throw new System.NotImplementedException();
        }

        public async Task<Photo> GetMainPhotoForUser(int userId)
        {
            return await _context.Photos.Where(u => u.UserID == userId).FirstOrDefaultAsync(p => p.IsMain);
        }

        public async Task<Photo> GetPhoto(int id)
        {
            var photo = await _context.Photos.FirstOrDefaultAsync(p => p.Id == id);
            return photo;
        }

        public async Task<User> GetUser(int id)
        {
            var user = await _context.Users.Include(p => p.Photos).FirstOrDefaultAsync(u => u.Id == id);
            return user;
        }

        public async Task<PagedList<User>> GetUsers(UserParams userParams)
        {
            var users = _context.Users.Include(u => u.Photos).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);

            var checkRole = _role.GetRole1(userParams.UserId);

            foreach (var item in checkRole)
            {
                if (item.Name != "Admin")
                {
                    users = users.Where(u => u.Gender == userParams.Gender);
                    if (userParams.MinAge != 18 || userParams.MaxAge != 100)
                    {
                        var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                        var maxDob = DateTime.Today.AddYears(-userParams.MinAge);
                        users = users.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);
                    }
                    if (userParams.CityId != null)
                    {
                        users = users.Where(u => u.CityId == userParams.CityId);
                    }

                }
                else
                    break;

            }



            return await PagedList<User>.CreatAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<PagedList<User>> GetUsersActive(UserParams userParams)
        {
            var users = _context.Users.Include(u => u.Photos).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);

            var checkRole = _role.GetRole1(userParams.UserId);

            if (userParams.CityId != 0)
            {
                users = users.Where(u => u.CityId == userParams.CityId);
            }

            if (userParams.MinAge != 18 || userParams.MaxAge != 100)
            {
                var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                var maxDob = DateTime.Today.AddYears(-userParams.MinAge);
                users = users.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);
            }

            foreach (var item in checkRole)
            {
                if (item.Name != "Admin")
                {
                    users = users.Where(u => u.Gender == userParams.Gender);

                }
                else
                    break;

            }

            users = users.Where(u => u.Status == UserStatus.Active);
            return await PagedList<User>.CreatAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<PagedList<User>> GetUsersInactive(UserParams userParams)
        {
            var users = _context.Users.Include(u => u.Photos).AsQueryable();

            users = users.Where(u => u.Id != userParams.UserId);

            users = users.Where(u => u.Status == UserStatus.Inactive);

            if (userParams.CityId != 0)
            {
                users = users.Where(u => u.CityId == userParams.CityId);
            }

            if (userParams.MinAge != 18 || userParams.MaxAge != 99)
            {
                var minDob = DateTime.Today.AddYears(-userParams.MaxAge - 1);
                var maxDob = DateTime.Today.AddYears(-userParams.MinAge);
                users = users.Where(u => u.DateOfBirth >= minDob && u.DateOfBirth <= maxDob);
            }

            return await PagedList<User>.CreatAsync(users, userParams.PageNumber, userParams.PageSize);
        }

        public async Task<bool> SaveAll()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update<T>(T entity) where T : class
        {
            throw new System.NotImplementedException();
        }
    }
}