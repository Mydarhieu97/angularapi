using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.Models
{
    public class User
    {
        public int Id { get; set; }       
        public string UserName { get; set; }
        public byte[] PassWordHash { get; set; }
        public byte[] PassWordSalt { get; set; }
        public int Gender { get; set; }
        public DateTime DateOfBirth {get; set;}
        public string KnownAs { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime LastActive { get; set; }
        public string Introduction { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public City City { get; set; }
        public ICollection<Photo> Photos { get; set; }
        public ICollection<UserRole> UserRoles { get; set; }
        public UserStatus Status { get; set; }

    }
}