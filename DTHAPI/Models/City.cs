using System.Collections.Generic;

namespace DTHAPI.Models
{
    public class City
    {

        public int CityId { get; set; }
        public string Title { get; set; }
        public int TotalDoanhNghiep { get; set; }
        public ICollection<User> Users { get; set; }
    }
}