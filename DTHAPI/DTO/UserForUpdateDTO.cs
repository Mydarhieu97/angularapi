using System;
using System.Collections.Generic;
using DTHAPI.Models;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.DTO
{
    public class UserForUpdateDTO
    {
        public string Introduction { get; set; }
        public int CityId { get; set; }

    }
}