using System;
using System.Collections.Generic;
using DTHAPI.Models;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.DTO
{
    public class UserForDeleteDTO
    {
        public UserStatus Status { get; set; }

    }
}