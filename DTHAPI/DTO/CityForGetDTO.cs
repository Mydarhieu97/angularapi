namespace DTHAPI.DTO
{
    public class CityForGetDTO
    {
        public int CityId { get; set; }
        public string Title { get; set; }
        public int TotalDoanhNghiep { get; set; }

    }
}