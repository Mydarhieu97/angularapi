using System;
using Microsoft.AspNetCore.Http;

namespace DTHAPI.DTO
{
    public class CityForCreateDTO
    {
        public string Title { get; set; }
        public int TotalDoanhNghiep { get; set; }
        
    }
}