using System;
using Microsoft.AspNetCore.Http;

namespace DTHAPI.DTO
{
    public class PhotoForCreationDTO
    {
        public string Url { get; set; }
        public IFormFile File { get; set; }
        public string  Descreption { get; set; }
        public DateTime DateAdded { get; set; }
        public string PublicId { get; set; }
        public PhotoForCreationDTO()
        {
            DateAdded = DateTime.Now;
        }
        
        
    }
}