using System.ComponentModel.DataAnnotations;

namespace DTHAPI.DTO
{
    public class UserForLoginDTO
    {
        public int Id { get; set; }
        [Display(Name="User's Name")]
        [Required]
        public string Username { get; set; }
        
        [Display(Name="Password")]
        [Required]
        public string Password { get; set; }
    }
}