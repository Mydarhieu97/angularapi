namespace DTHAPI.DTO
{
    public class DTHEnum
    {
        public enum Gender
        {
            Female = 0,
            Male = 1
        }
        public enum UserStatus
        {
            Inactive = 0,
            Active = 1
        }
    }
}