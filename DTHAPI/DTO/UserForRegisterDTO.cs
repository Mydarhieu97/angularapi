using System;
using System.ComponentModel.DataAnnotations;

namespace DTHAPI.DTO
{
    public class UserForRegisterDTO
    {
        [Display(Name="User's Name")]
        [Required]
        public string UserName {get; set;}
        [StringLength(255, MinimumLength = 8, ErrorMessage = "Your Password Must Be More 8 Characters")]
        [Display(Name="Password")]
        [Required]
        public string PassWord { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("PassWord", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Gender")]
        [Required]
        public int Gender { get; set; }

        [DataType(DataType.DateTime)]
        [Display(Name = "Date Of Birth")]
        [Required]
        public DateTime DateOfBirth {get; set;}
        
        [Display(Name = "City")]
        [Required]
        public int? CityId { get; set; }
        
    }
}