using System;
using System.Collections.Generic;
using DTHAPI.Models;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.DTO
{
    public class UserForDetailDTO
    {
        public int Id { get; set; }       
        public string UserName { get; set; }
        public int Gender { get; set; }
        public int Age {get; set;}
        public string KnownAs { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime LastActive { get; set; }
        public string Introduction { get; set; }
        public string Address { get; set; }
        public int CityId { get; set; }
        public string PhotoURL { get; set; }
        public ICollection<PhotoForDetailDTO> Photos { get; set; }
        public UserStatus Status { get; set; }

    }
}