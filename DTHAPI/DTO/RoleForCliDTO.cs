using System;
using System.Collections.Generic;
using DTHAPI.Models;
using static DTHAPI.DTO.DTHEnum;

namespace DTHAPI.DTO
{
    public class RoleForCliDTO
    {      
        public string Name { get; set; }
        
    }
}