import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';
import { User } from 'src/app/_models/user';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { Pagination, PaginatedResult } from 'src/app/_models/pagination';
import { AlertifyService } from 'src/app/_services/alertify.service';


@Component({
  selector: 'app-member-disabled',
  templateUrl: './member-disabled.component.html',
  styleUrls: ['./member-disabled.component.css']
})

export class MemberDisabledComponent implements OnInit {
  baseUrl = environment.getUrl;
  usersInactive: User[];
  pagination: Pagination;
  constructor(private authService: AuthService,
    private alertify: AlertifyService,
    public userService: UserService,
    private route: ActivatedRoute) { }

  // ngOnInit() {

  //   this.route.data.subscribe(data => {
  //     let arr = data['users'];
  //     var obShow = [];
  //     for(let i = 0; i < arr.length; i++)
  //     {
  //       if(arr[i].status === 0){
  //         obShow.push(arr[i]);
  //       }
  //     }
  //     this.users = obShow;
  //   });

  // }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.usersInactive = data['usersInactive'].result;
      this.pagination = data['usersInactive'].pagination;
    });

     
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadUsers();
  }


  loadUsers() {
    this.userService.getUsersInactive(this.pagination.currentPage, this.pagination.itemsPerPage).subscribe((res: PaginatedResult<User[]>) => {
      this.usersInactive = res.result;
      this.pagination = res.pagination;
    }, error => {
      this.alertify.error(error);
    });
  }
}

