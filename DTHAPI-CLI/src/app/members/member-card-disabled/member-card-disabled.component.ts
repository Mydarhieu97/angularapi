import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/_models/user';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-member-card-disabled',
  templateUrl: './member-card-disabled.component.html',
  styleUrls: ['./member-card-disabled.component.css']
})
export class MemberCardDisabledComponent implements OnInit {

  @Input() user: User;
  @Output() userid = new EventEmitter<number>();

  constructor(private authService: AuthService,
    public userService: UserService,
    private alertify: AlertifyService
    ) { }

  ngOnInit() {
   
  }

  realDeleteUser(id: number) {
    this.alertify.confirm("WARNING: All information of this user will be delete forever and you cannot restore them, this is not recommended!!! Are you sure that you want delete this member?", () => {
      this.userService.realDeleteUser(id).subscribe(()=> {
        this.alertify.success("User has been deleted");
        window.location.reload();
      }, error => {
        this.alertify.error(error);
      });
    });
    
  }


  enableUser(id: number) {
    this.alertify.confirm("NOTE: User can login after be enable! Are you sure that you want enable this member?", () => {
      this.userService.enableUser(id).subscribe(()=> {
        this.userid = Number.bind(id);
        this.alertify.success("User has been enable");
        window.location.reload();
      }, error => {
        this.alertify.error(error);
      });
    });
  }

  refresh(): void {
    window.location.reload();
}

}
