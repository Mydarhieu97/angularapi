import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/_models/user';
import { UserService } from 'src/app/_services/user.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { Gender, UserStatus } from 'src/app/_models/dthEnum';
import { City } from 'src/app/_models/city';
import { CityService } from 'src/app/_services/city.service';
import { AuthService } from 'src/app/_services/auth.service';



@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})
export class MemberDetailComponent implements OnInit {
  user: User;
  city: City;
  @Output() userid = new EventEmitter<number>();
  genEnum = Gender;
  statusEnum = UserStatus;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];


  constructor(private userService: UserService,
    private alertify: AlertifyService,
    private route: ActivatedRoute,
    private router: Router,
    private cityService: CityService,
    public authService: AuthService) { }
  ngOnInit() {
    this.route.data.subscribe(data => {
      this.user = data['user'];
    });
    this.cityService.getCity(this.user.id).subscribe(response => {
      this.city = response;
      this.user.cityName = this.city.title;
    }
    );
    

    this.galleryOptions = [
      {
        width: '500px',
        height: '500px',
        imagePercent: 100,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        preview: false
      }
    ];
    this.galleryImages = this.getImages();

  }
  getImages() {
    const imageUrls = [];
    for (let i = 0; i < this.user.photos.length; i++) {
      imageUrls.push({
        small: this.user.photos[i].url,
        medium: this.user.photos[i].url,
        big: this.user.photos[i].url,
        description: this.user.photos[i].descreption
      });
    }
    return imageUrls;
  }
  
  disableUser(id: number) {
    this.alertify.confirm("NOTE: User cannot login after be disable! Are you sure that you want disable this member?", () => {
      this.userService.disableUser(id).subscribe(()=> {
        this.alertify.success("User has been disable");
        // window.location.reload();
        this.router.navigate(["/members"]);
      }, error => {
        this.alertify.error(error);
      });
    });
  }

  realDeleteUser(id: number) {
    this.alertify.confirm("WARNING: All information of this user will be delete forever and you cannot restore them, this is not recommended!!! Are you sure that you want delete this member?", () => {
      this.userService.realDeleteUser(id).subscribe(()=> {
        this.alertify.success("User has been deleted");
        this.router.navigate(["/members"]);
      }, error => {
        this.alertify.error(error);
      });
    });
    
  }

  enableUser(id: number) {
    this.alertify.confirm("NOTE: User can login after be enable! Are you sure that you want enable this member?", () => {
      this.userService.enableUser(id).subscribe(()=> {
        this.userid = Number.bind(id);
        this.alertify.success("User has been enable");
        this.router.navigate(["/member/disabled"])
        // window.location.reload();
      }, error => {
        this.alertify.error(error);
      });
    });
  }

}
