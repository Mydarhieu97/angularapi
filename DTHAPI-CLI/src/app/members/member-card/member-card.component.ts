import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/_models/user';
import { AuthService } from 'src/app/_services/auth.service';
import { UserService } from 'src/app/_services/user.service';
import { AlertifyService } from 'src/app/_services/alertify.service';

@Component({
  selector: 'app-member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.css']
})
export class MemberCardComponent implements OnInit {
  @Input() user: User;

  constructor(public authService: AuthService,
    public userService: UserService,
    private alertify: AlertifyService
    ) { }

  ngOnInit() {
   
  }


  disableUser(id: number) {
    this.alertify.confirm("NOTE: User cannot login after be disable! Are you sure that you want disable this member?", () => {
      this.userService.disableUser(id).subscribe(()=> {
        this.alertify.success("User has been disable");
        window.location.reload();
      }, error => {
        this.alertify.error(error);
      });
    });
  }

  

}
