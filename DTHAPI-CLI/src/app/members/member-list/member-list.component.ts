import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertifyService } from '../../_services/alertify.service';
import { environment } from 'src/environments/environment';
import { User } from '../../_models/user';
import { UserService } from '../../_services/user.service';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/_services/auth.service';
import { Pagination, PaginatedResult } from 'src/app/_models/pagination';
import { CitySelect2 } from 'src/app/_models/cityselect2';

@Component({
  selector: 'app-member-list',
  templateUrl: './member-list.component.html',
  styleUrls: ['./member-list.component.css']
})

export class MemberListComponent implements OnInit {
 baseUrl = environment.getUrl;
 users: User[];
 userParams: any = {};
 pagination: Pagination;
 cities: CitySelect2[];
  constructor(private http: HttpClient, private alertify: AlertifyService,
    private userService: UserService,
    public authService: AuthService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.users = data['users'].result;
      this.pagination = data['users'].pagination;
    });

    this.route.data.subscribe(data => {
      this.cities = data['cities'];
    });

    console.log(this.cities);

    this.userParams.gender = this.authService.gender === 1 ? 0 : 1;
    this.userParams.minAge = 18;
    this.userParams.maxAge = 99;
    this.userParams.cityId = 0;

     
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadUsers();
  }

  resetFilters() {
    this.userParams.gender = this.authService.gender === 1 ? 0 : 1;
    this.userParams.minAge = 18;
    this.userParams.maxAge = 99;
    this.userParams.cityId = 0;
    this.loadUsers();
  }


  loadUsers() {
    this.userService.getUsers(this.pagination.currentPage, this.pagination.itemsPerPage, this.userParams).subscribe((res: PaginatedResult<User[]>) => {
      this.users = res.result;
      this.pagination = res.pagination;
    }, error => {
      this.alertify.error(error);
    });
  }
}
