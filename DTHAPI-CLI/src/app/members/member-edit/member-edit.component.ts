import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { User } from 'src/app/_models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/_services/user.service';
import { AuthService } from 'src/app/_services/auth.service';
import { UserStatus, Gender } from 'src/app/_models/dthEnum';
import { City } from 'src/app/_models/city';

@Component({
  selector: 'app-member-edit',
  templateUrl: './member-edit.component.html',
  styleUrls: ['./member-edit.component.css']
})
export class MemberEditComponent implements OnInit {
  @ViewChild("editForm", { static: false }) editForm: NgForm;
  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (this.editForm.dirty) {
      $event.returnValue = true;
    }
  }
  user: User;
  cities: City[];
  photoUrl: string;
  statusEnum = UserStatus;
  genEnum = Gender;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  constructor(private route: ActivatedRoute,
    private alertify: AlertifyService,
    private userService: UserService,
    private authService: AuthService) { }

  ngOnInit() {

    this.route.data.subscribe(data => {
      this.cities = data['cities'];
    });
    
    this.route.data.subscribe(data => {
      this.user = data['user'];
      const cityName = this.cities.filter(
        book => book.cityId === this.user.cityId);
      this.user.cityName = cityName[0].title;
    });

    this.authService.currentPhoto.subscribe(photoUrl => this.photoUrl = photoUrl);

    this.galleryOptions = [
      {
        width: '500px',
        height: '500px',
        imagePercent: 100,
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Slide,
        preview: false
      }
    ];
    this.galleryImages = this.getImages();
  }

  getImages() {
    const imageUrls = [];
    for (let i = 0; i < this.user.photos.length; i++) {
      imageUrls.push({
        small: this.user.photos[i].url,
        medium: this.user.photos[i].url,
        big: this.user.photos[i].url,
        description: this.user.photos[i].descreption
      });
    }
    return imageUrls;
  }

  updateUser() {
    console.log(this.user);
    this.userService.updateUser(this.authService.decodedToken.nameid, this.user).subscribe(next => {
      this.alertify.success("Profile updated successfully");
      this.route.data.subscribe(data => {
        this.cities = data['cities'];
        this.route.data.subscribe(data => {
          this.user = data['user'];
          const cityName = this.cities.filter(
            book => book.cityId === +this.user.cityId);
          this.user.cityName = cityName[0].title;
        });
      });
      this.editForm.reset(this.user);
    }, error => {
      this.alertify.error(error);
    });

  }


  updateMainPhoto(photoUrl) {
    this.user.photoURL = photoUrl;
  }

}
