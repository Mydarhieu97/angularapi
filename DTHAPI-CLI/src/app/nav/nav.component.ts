import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  model: any = {};
  photoUrl: string;
  constructor(public authService: AuthService,
    private alertify: AlertifyService,
    private router: Router,
    ) { }

  ngOnInit() {
    this.authService.currentPhoto.subscribe(photoUrl => this.photoUrl = photoUrl);

  }



  login() {
    this.authService.login(this.model).subscribe(next => {
    }, error => {
      this.alertify.error(error);
    }, () => {

      if(!this.authService.statusCheck())
      {
        this.alertify.alert("Your account has been locked, please contact Admin !!!", () => {});
        localStorage.removeItem('token');
        this.router.navigate(['/home']);
      }
      this.router.navigate(['/home']);
    }
    );

  }
  
  loggedIn(){
    return this.authService.loggedIn();
  }
  logout(){
    this.alertify.confirm('Are You Sure That You Want To Logged Out',
     () => {this.alertify.warning('Logged Out'),
     localStorage.removeItem('token'), 
     this.router.navigate(['/home'])});
  }
}
