import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { AlertifyService } from '../_services/alertify.service';;
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { City } from '../_models/city';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { Select2OptionData } from 'ng-select2';
import { CitySelect2 } from '../_models/cityselect2';
import { User } from '../_models/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  @Output() cancelRegister = new EventEmitter();
  user: User;
  registerForm: FormGroup;
  cities: CitySelect2[];

  constructor(public authservice: AuthService, 
    private alertify: AlertifyService, 
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder
    ) { }

  ngOnInit() {

    // this.registerForm = new FormGroup({
    //   username: new FormControl('', Validators.required),
    //   password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    //   confirmpassword: new FormControl('', Validators.required),
    //   gender: new FormControl('0', Validators.required),
    //   dateOfBirth: new FormControl('2000-01-01', Validators.required),
    //   cityId: new FormControl('35', Validators.required)
    // }, this.passwordMatchValidator);
    this.createRegisterForm();
  
    this.route.data.subscribe(data => {
      this.cities = data['cities'];
    });

  }


  createRegisterForm() {
    this.registerForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmpassword: ['', Validators.required],
      gender: ['1', Validators.required],
      dateOfBirth: ['', Validators.required],
      cityId: ['35', Validators.required]
    }, {validator: this.passwordMatchValidator});
  }


  passwordMatchValidator(g: FormGroup) {
    return g.get('password').value == g.get('confirmpassword').value ? null : {'mismatch': true};
  }


  register() {

    if(this.registerForm.valid)
    {
      const formatDate = moment(this.registerForm.value.dateOfBirth).format('MM/DD/YYYY');
      this.registerForm.value.dateOfBirth = formatDate;
      this.user = Object.assign({}, this.registerForm.value);
      this.authservice.register(this.user).subscribe(() => {
        this.alertify.success('Registration Successfully');
      }, error => {
        this.alertify.error(error);
      }, () => {
        this.authservice.login(this.user).subscribe(()=>{
          this.router.navigate(["/members"]);
        });
      });
    }
    
  }

  cancel() {
    this.cancelRegister.emit(false);
    this.alertify.warning('cancelled');
  }

}
