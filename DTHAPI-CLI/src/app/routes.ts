import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MemberListComponent } from './members/member-list/member-list.component';
import { MessagesComponent } from './messages/messages.component';
import { ListsComponent } from './lists/lists.component';
import { AuthGuard } from './_guards/auth.guard';
import { MemberDetailComponent } from './members/member-detail/member-detail.component';
import { MemberDetailResolver } from './_resolvers/member-detail.resolver';
import { MemberListResolver } from './_resolvers/member-list.resolver';
import { ShopGuard } from './_guards/shop.guard';
import { AdminGuard } from './_guards/admin.guard';
import { MemberEditComponent } from './members/member-edit/member-edit.component';
import { MemberEditResolver } from './_resolvers/member-edit.resolver';
import { PreventUnsavedChanges } from './_guards/prevent-unsaved-changes.guard';
import { MemberDisabledComponent } from './members/member-disabled/member-disabled.component';
import { CityListResolver } from './_resolvers/city-list.resolver';
import { RegisterComponent } from './register/register.component';
import { CityForRegisterResolver } from './_resolvers/city-list-for-register.resolver';
import { MemberListInactiveResolver } from './_resolvers/member-list-inactive.resolver';

export const appRoutes: Routes = [
    
    { path: '', component: HomeComponent, resolve: {cities: CityForRegisterResolver}},
    {
        path: '',
        runGuardsAndResolvers: 'always',
        canActivate: [AuthGuard],
        children: [
            { path: 'member/edit', canDeactivate: [PreventUnsavedChanges], component: MemberEditComponent, resolve: {user: MemberEditResolver, cities: CityListResolver}},
            { path: 'members', component: MemberListComponent, resolve: {users: MemberListResolver, cities: CityForRegisterResolver}},
            { path: 'members/:id', component: MemberDetailComponent, resolve: {user: MemberDetailResolver}},
            { path: 'messages',canActivate: [ShopGuard], component: MessagesComponent},
            { path: 'lists', component: ListsComponent},
            { path: 'member/disabled', canActivate:[AdminGuard], component: MemberDisabledComponent, resolve: {usersInactive: MemberListInactiveResolver}},
        ]
    },
    { path:'register', component: RegisterComponent, resolve: {cities: CityForRegisterResolver}},
    { path: '**', redirectTo: '', pathMatch: 'full'}
];