export enum Gender {
    Female = 0,
    Male = 1
}
export enum UserStatus {
    Inactive = 0,
    Active = 1
}