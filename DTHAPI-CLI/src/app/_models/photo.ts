export interface Photo {
    id: number;
    url: string;
    dateAdded: Date;
    descreption: string;
    isMain: boolean;
}
