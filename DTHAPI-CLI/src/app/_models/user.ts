import { Photo } from './photo';
import { UserStatus, Gender } from './dthEnum';

export interface User {
    id: number;
    userName: string;
    knownAs: string;
    age: number;
    gender: Gender;
    createAt: Date;
    lastActive: Date;
    photoURL: string;
    introduction?: string;
    address: string;
    cityId: number;
    cityName: string;
    status: UserStatus;
    photos?: Photo[];
}
