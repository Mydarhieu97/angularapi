import { User } from './user';

export interface City {
    cityId: number;
    title: string;
    totalDoanhNghiep: number;
    user: User[];
}
