import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../_models/user';
import { AlertifyService } from './alertify.service';
import { Router } from '@angular/router';
import { PaginatedResult } from '../_models/pagination';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient, 
  private alertify: AlertifyService,
  private router: Router) { }


getUsers(page?, itemsPerPage?, userParams?): Observable<PaginatedResult<User[]>> 
{

  const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
  let params = new HttpParams();
  if(page != null && itemsPerPage != null)
  {
    params = params.append("pageNumber", page);
    params = params.append("pageSize", itemsPerPage);
  }

  if(userParams != null)
  {

    params = params.append("minAge", userParams.minAge);
    params = params.append("maxAge", userParams.maxAge);
    params = params.append("gender", userParams.gender);
    params = params.append("cityId", userParams.cityId);

  }

  return this.http.get<User[]>(this.baseUrl + 'users', {observe: 'response', params})
  .pipe(
    map(response => {
      paginatedResult.result = response.body;
      if(response.headers.get('Pagination') != null)
      {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
      }
      return paginatedResult;
    })
  );

}

getUsersInactive(page?, itemsPerPage?): Observable<PaginatedResult<User[]>> 
{

  const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
  let params = new HttpParams();
  if(page != null && itemsPerPage != null)
  {
    params = params.append("pageNumber", page);
    params = params.append("pageSize", itemsPerPage);
  }
  return this.http.get<User[]>(this.baseUrl + 'users/inactive', {observe: 'response', params})
  .pipe(
    map(response => {
      paginatedResult.result = response.body;
      if(response.headers.get('Pagination') != null)
      {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
      }
      return paginatedResult;
    })
  );

}


getUser(id): Observable<User> {

  return this.http.get<User>(this.baseUrl + 'users/' + id);

}
updateUser(id: number, user: User) {
  return this.http.put(this.baseUrl + 'users/' + id, user);
}

disableUser(id: number) {
  return this.http.delete(this.baseUrl + 'users/' + id); 
}

enableUser(id: number) {
  return this.http.delete(this.baseUrl + 'users/enable/' + id);
}

realDeleteUser(id: number) {
   return this.http.delete(this.baseUrl + 'users/delete/' + id);
}
  
    

setMainPhoto(userId: number, id: number) {
  return this.http.post(this.baseUrl + 'users/' + userId + '/photos/' + id + '/setmain', {});
}

deletePhoto(userId: number, id: number) {
  return this.http.delete(this.baseUrl + 'users/' + userId + '/photos/' + id);
}

}
