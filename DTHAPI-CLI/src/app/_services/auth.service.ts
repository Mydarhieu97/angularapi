import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserService } from './user.service';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from './alertify.service';
import * as jwt_decode from 'jwt-decode';
import { User } from '../_models/user';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl = 'http://localhost:5000/api/author/';
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  user: User;
  status: number;
  gender: number;
  photo: any;
  photoUrl = new BehaviorSubject<string>('../../assets/user.png');
  currentPhoto = this.photoUrl.asObservable();

  constructor(private http: HttpClient, private userService: UserService) { }


  changeMemberPhoto(photoUrl: string) {
    this.photoUrl.next(photoUrl);

  }

  login(model: any) {
    return this.http.post(this.baseUrl + 'login', model).pipe(
      map((response: any) => {
        const user = response;
        if (user)
          localStorage.setItem('token', user.token);
          localStorage.setItem('photoURL', user.photoURL);
          localStorage.setItem('status', user.status);
          localStorage.setItem('status', user.gender);
        this.decodedToken = this.jwtHelper.decodeToken(user.token);
        this.photo = user.photoURL;
        this.status = user.status;
        this.gender = user.gender;
        this.changeMemberPhoto(this.photo);
      })
    );
    
  }


  register(model: any) {
    return this.http.post(this.baseUrl + 'register', model);
  }


  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  statusCheck() {
    if(this.status == 1)
    return true;
    return false;
  }

  roleAdmin()
  {
    const token = localStorage.getItem('token');
    var decoded = jwt_decode(token);
    var isAdmin = decoded['role'];
    var str = isAdmin.split(",");
    for (let i = 0; i <= str.length; i++) {
      if (str[i] === "Admin")
        return true;
    }
    return false;
  }

  roleShop()
  {
    const token = localStorage.getItem('token');
    var decoded = jwt_decode(token);
    var isShop = decoded['role'];
    var str = isShop.split(",");
    for (let i = 0; i <= str.length; i++) {
    if (str[i] === "Shop")
      return true;
    }
    return false;
  }

  roleUser()
  {
    const token = localStorage.getItem('token');
    var decoded = jwt_decode(token);
    var isAdmin = decoded['role'];
    var str = isAdmin.split(",");
    for (let i = 0; i <= str.length; i++) {
      if (str[i] === "Users")
        return true;
    }
    return false;
  }



}
