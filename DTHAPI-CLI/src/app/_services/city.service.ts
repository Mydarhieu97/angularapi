import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { City } from '../_models/city';
import { Observable } from 'rxjs';
import { CitySelect2 } from '../_models/cityselect2';

@Injectable({
  providedIn: 'root'
})
export class CityService {

constructor(private http: HttpClient) { }

getCities(): Observable<City[]> 
  {
    return this.http.get<City[]>('http://localhost:5000/api/values');
  }

getCity(id: number): Observable<City>
  {
    return this.http.get<City>('http://localhost:5000/api/values/' + id);
  }

getCityForRegister(): Observable<CitySelect2[]>
  {
    return this.http.get<CitySelect2[]>('http://localhost:5000/api/values/citynew');
  }

}
