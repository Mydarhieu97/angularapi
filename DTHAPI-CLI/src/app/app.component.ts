import { Component, OnInit } from '@angular/core';
import { AuthService } from './_services/auth.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import { City } from './_models/city';
import { CityService } from './_services/city.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  jwtHelper = new JwtHelperService();


  constructor(private authService: AuthService, private cityService: CityService) {}
  
  
  ngOnInit() {
    
      const token = localStorage.getItem('token');
      const photoURL = localStorage.getItem('photoURL');
      const status = localStorage.getItem('status');
      const gender = localStorage.getItem('gender');

      if(token){
        this.authService.decodedToken = this.jwtHelper.decodeToken(token);
      }

      if(photoURL){
        this.authService.photo = photoURL;
        this.authService.changeMemberPhoto(photoURL);
      }

      if(status){
        this.authService.status = Number.parseInt(status);
      }

      if(gender){
        this.authService.gender = Number.parseInt(gender);
      }
      

  } 
}
