import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { City } from '../_models/city';
import { CityService } from '../_services/city.service';

@Injectable()

export class CityListResolver implements Resolve<City[]> {
    constructor (
        private cityService: CityService,
        private router: Router,
        private alertify: AlertifyService) {}
    

    resolve(route: ActivatedRouteSnapshot) : Observable<City[]> {
        return this.cityService.getCities().pipe(
            catchError(error => {
                this.alertify.error("Cancel Retrieving Data");
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }

} 