import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { City } from '../_models/city';
import { CityService } from '../_services/city.service';
import { CitySelect2 } from '../_models/cityselect2';

@Injectable()

export class CityForRegisterResolver implements Resolve<CitySelect2[]> {
    constructor (
        private cityService: CityService,
        private router: Router,
        private alertify: AlertifyService) {}
    

    resolve(route: ActivatedRouteSnapshot) : Observable<CitySelect2[]> {
        return this.cityService.getCityForRegister().pipe(
            catchError(error => {
                this.alertify.error("Cancel Retrieving Data");
                this.router.navigate(['/home']);
                return of(null);
            })
        );
    }

} 